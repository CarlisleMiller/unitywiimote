﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System;
using WiimoteApi;

public class CallibrateWiimote : MonoBehaviour
{

    public RectTransform[] ir_dots;
    public RectTransform[] ir_bb;
    public RectTransform ir_pointer;

    private Wiimote wiimote;

    private Vector3 wmpOffset = Vector3.zero;

    public Vector2 topLeftPosition;
    public Vector2 topRightPosition;
    public Vector2 bottomLeftPosition;
    public Vector2 bottomRightPosition;
    private Vector2 callibrationMarker;

    bool triggerPressed = false;
    bool calSetupPressed = false;
    bool currentlyCalibrating = false;

    int calibrationStep = 0;

    void Start()
    {
        StartCoroutine(SetupWiiRemote());
    }

    IEnumerator SetupWiiRemote()
    {
        WiimoteManager.FindWiimotes(); // Connect to wii mote
        yield return new WaitForSeconds(2);
        wiimote.SendPlayerLED(true, false, false, false); // Set LED to player 1 to stop the LEDs blinking
        wiimote.SetupIRCamera(IRDataType.BASIC); // Setup IR sensor bar
    }

    // Checks if the "B" button was pressed
    void WasTriggerPressed()
    {
        if (wiimote.Button.b)
        {
            if (triggerPressed == false)
            {
                triggerPressed = true;
                Debug.Log("Trigger pressed");

                if (currentlyCalibrating == true)
                {
                    CallibrationSetup();    // Calibration Setup
                }
                else
                {
                    float[] pointer = wiimote.Ir.GetPointingPosition();
                    Debug.Log("x: " + pointer[0] + "  y: " + pointer[1]);
                    //wiimote.RumbleOn = true; // Enable Rumble
                    //wiimote.SendStatusInfoRequest();
                    // Fire shotgun shot at this location
                    // Vibrate the wiimote
                }
            }
        }
        else
        {
            triggerPressed = false;
            //wiimote.RumbleOn = false; // Disable Rumble
            //wiimote.SendStatusInfoRequest();
        }
    }



    // Checks if the "A" button was pressed
    void WasCallibrationSetupPressed()
    {
        if (wiimote.Button.a)
        {
            if (calSetupPressed == false)
            {
                calSetupPressed = true;
                currentlyCalibrating = true;
                Debug.Log("Calibration setup started");
                Debug.Log("Aim and shoot at the top left corner");
            }
        }
        else
        {
            calSetupPressed = false;
        }
    }

    // Store coordinates of the corner shot while calibrating
    void ShootCalibrationMarker()
    {
        float[] pointer = wiimote.Ir.GetPointingPosition();
        Debug.Log("x: " + pointer[0] + "  y: " + pointer[1]);
        callibrationMarker.Set(pointer[0], pointer[1]);
        calibrationStep += 1;
    }

    // Setup callibration via shooting the four corners
    void CallibrationSetup()
    {
        if (calibrationStep == 0)
        {
            ShootCalibrationMarker();
            topLeftPosition = callibrationMarker;
            Debug.Log("Aim and shoot at the top right corner");
        }
        else if (calibrationStep == 1)
        {
            ShootCalibrationMarker();
            topRightPosition = callibrationMarker;
            Debug.Log("Aim and shoot at the bottom left corner");
        }
        else if (calibrationStep == 2)
        {
            ShootCalibrationMarker();
            bottomLeftPosition = callibrationMarker;
            Debug.Log("Aim and shoot at the bottom right corner");
        }
        else if (calibrationStep == 3)
        {
            ShootCalibrationMarker();
            bottomRightPosition = callibrationMarker;
            currentlyCalibrating = false;
            Debug.Log("Calibration Complete");
            Debug.Log("Top Left: " + topLeftPosition + "  Top Right: " + topRightPosition + "  Bottom Left: " + bottomLeftPosition + "  BottomRight: " + bottomRightPosition);
        }
    }

    //void TestLoop()
    //{
    //    if (currentlyCalibrating == true)
    //    { 
    //        for (int i = 0; i < 4; i++)
    //        {
    //            Debug.Log("i is: " + i);
    //            callibrationMarker = ir_pointer.anchorMin;
    //            Debug.Log("Marker set at: " + callibrationMarker);
    //            ShootCalibrationMarker();
    //            i += 1;
    //        }
    //    }
    //}




    void Update()
    {
        if (!WiimoteManager.HasWiimote()) { return; }

        wiimote = WiimoteManager.Wiimotes[0];

        int ret;
        do
        {
            ret = wiimote.ReadWiimoteData();

        } while (ret > 0); // While reading from the wiimote

        WasCallibrationSetupPressed();
        WasTriggerPressed();

        if (ir_dots.Length < 4) return;

        float[,] ir = wiimote.Ir.GetProbableSensorBarIR();
        for (int i = 0; i < 2; i++)
        {
            float x = (float)ir[i, 0] / 1023f;
            float y = (float)ir[i, 1] / 767f;
            if (x == -1 || y == -1)
            {
                ir_dots[i].anchorMin = new Vector2(0, 0);
                ir_dots[i].anchorMax = new Vector2(0, 0);
            }

            ir_dots[i].anchorMin = new Vector2(x, y);
            ir_dots[i].anchorMax = new Vector2(x, y);

            if (ir[i, 2] != -1)
            {
                int index = (int)ir[i, 2];
                float xmin = (float)wiimote.Ir.ir[index, 3] / 127f;
                float ymin = (float)wiimote.Ir.ir[index, 4] / 127f;
                float xmax = (float)wiimote.Ir.ir[index, 5] / 127f;
                float ymax = (float)wiimote.Ir.ir[index, 6] / 127f;
                ir_bb[i].anchorMin = new Vector2(xmin, ymin);
                ir_bb[i].anchorMax = new Vector2(xmax, ymax);
            }
        }

        float[] pointer = wiimote.Ir.GetPointingPosition();
        ir_pointer.anchorMin = new Vector2(pointer[0], pointer[1]);
        ir_pointer.anchorMax = new Vector2(pointer[0], pointer[1]);
    }

    void OnApplicationQuit()
    {
        if (wiimote != null)
        {
            WiimoteManager.Cleanup(wiimote);
            wiimote = null;
        }
    }
}
